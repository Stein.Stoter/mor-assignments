% Supporting material for the youtube lecture series
% "Model Order Reduction in Computational Solid Mechanics"
% accessible at youtube.com/playlist?list=PLMHTjE57oyvqbTrSo_e6x5rdGpp3ncgX8
% Author:                          Stein Stoter
% Email:                           k.f.s.stoter@tue.nl

function main

close all
clear
clc

%Input parameters
W = 4; % Width of the square domain
NrEls = 4; % Elements in X (and Y) direction of square 1
NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2

%Get global stiffness matrix and force vector
[K1,F1] = ComputeStiffness(W,NrEls,1);
[K2,F2] = ComputeStiffness(W/4,NrEls2,2);
[K3,F3] = ComputeStiffness(W,NrEls,3);

%Determine nodes that need to be condensed for each square 
nodes_c1 = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
nodes_c2_l = 1:NrEls2+1:(NrEls2+1)*(NrEls2+1);
nodes_c2_r = NrEls2+1:NrEls2+1:(NrEls2+1)*(NrEls2+1);
nodes_c2 = horzcat(nodes_c2_l,nodes_c2_r);
nodes_c3 = 1:NrEls+1:(NrEls+1)*(NrEls2+1);

%Perform static condensation for each substructure
[Kcc_hat1,Fc_hat1,invKbb1,Kbc1,Fb1] = StaticallyCondensate(K1,F1,nodes_c1);
[Kcc_hat2,Fc_hat2,invKbb2,Kbc2,Fb2] = StaticallyCondensate(K2,F2,nodes_c2);
[Kcc_hat3,Fc_hat3,invKbb3,Kbc3,Fb3] = StaticallyCondensate(K3,F3,nodes_c3);

%Assemble the global statically condensated system
K = Kcc_hat2;
K(1:2:(NrEls2+1)*2,1:2:(NrEls2+1)*2) = K(1:2:(NrEls2+1)*2,1:2:(NrEls2+1)*2) + Kcc_hat1;
K(2:2:(NrEls2+1)*2,2:2:(NrEls2+1)*2) = K(2:2:(NrEls2+1)*2,2:2:(NrEls2+1)*2) + Kcc_hat3;
F = Fc_hat2;
F(1:2:(NrEls2+1)*2) = F(1:2:(NrEls2+1)*2)+ Fc_hat1;
F(2:2:(NrEls2+1)*2) = F(2:2:(NrEls2+1)*2)+ Fc_hat3;

%Solve system of equation
uc2 = K\F;
ub2 = invKbb2*Fb2-invKbb2*Kbc2*uc2;
uc1 = uc2(1:2:(NrEls2+1)*2);
ub1 = invKbb1*Fb1-invKbb1*Kbc1*uc1;
uc3 = uc2(2:2:(NrEls2+1)*2);
ub3 = invKbb3*Fb3-invKbb3*Kbc3*uc3;

%Restructure the solution vectors
u1 = ReverseRestructureRows( vertcat(ub1,uc1) ,nodes_c1);
u2 = ReverseRestructureRows( vertcat(ub2,uc2) ,nodes_c2);
u3 = ReverseRestructureRows( vertcat(ub3,uc3) ,nodes_c3);

%Plot results
figure(1)
PlotRectangle(u2,W/4,W/4, W/NrEls, W,0)

%Plot results
figure('Position', [200 200 1150 450])
PlotRectangle(u1,W,W, W/NrEls, 0,0)
hold on
PlotRectangle(u2,W/4,W/4, W/NrEls, W,0)
hold on
PlotRectangle(u3,W,W, W/NrEls, W+W/4,0)
zlim([0,10])
end

function PlotRectangle(u,W,H,h,x_ll,y_ll)
    %Plot a solution vector 'u' on a rectangular domain WxH with element
    %size h and with lower-left coordinate (x_ll,y_ll) 
    [X,Y] = meshgrid( x_ll:h:x_ll+W , y_ll:h:y_ll+H );
    Z = reshape(u,size(X'))';
    surf(X,Y,Z)
end


function [Kcc_hat,Fc_hat,invKbb,Kbc,Fb] = StaticallyCondensate(K,F,nodes_c)
    %This function performs the static condensation procedure on matrix K 
    %and vector F with the node numbers of the to-be-condensed nodes 'nodes_c'
    %It returns the new relevant matrices needed for computing u_c and u_b
    
    %Restructure matrix such that condensable nodes are at the end
    K = RestructureRows(K,nodes_c); %Relocate relevant rows of K
    K = RestructureRows(K',nodes_c)'; %Relocate relevant columns of K
    F = RestructureRows(F,nodes_c); %Relocate relevant rows of F

    %Splice matrices
    NrEls = sqrt(length(F))-1;
    Kbb = K(1:(NrEls+1)*(NrEls+1)-length(nodes_c) , 1:(NrEls+1)*(NrEls+1)-length(nodes_c)); % Top left submatrix
    Kbc = K(1:(NrEls+1)*(NrEls+1)-length(nodes_c) , (NrEls+1)*(NrEls+1)-length(nodes_c)+1:(NrEls+1)*(NrEls+1));  %Top right submatrix
    Kcb = K((NrEls+1)*(NrEls+1)-length(nodes_c)+1:(NrEls+1)*(NrEls+1) , 1:(NrEls+1)*(NrEls+1)-length(nodes_c) );  %Lower left submatrix
    Kcc = K((NrEls+1)*(NrEls+1)-length(nodes_c)+1:(NrEls+1)*(NrEls+1) , (NrEls+1)*(NrEls+1)-length(nodes_c)+1:(NrEls+1)*(NrEls+1));  %Lower right submatrix
    Fb = F(1:(NrEls+1)*(NrEls+1)-length(nodes_c)); %Top subvector
    Fc = F((NrEls+1)*(NrEls+1)-length(nodes_c)+1:(NrEls+1)*(NrEls+1)); %Bottom subvector

    %Compute Schur complements
    invKbb = inv(Kbb);
    Kcc_hat = Kcc-Kcb*invKbb*Kbc;
    Fc_hat = Fc - (Kcb*invKbb*Fb);
end

function K = RestructureRows(A,nodes_c)
    %Put the row numbers from 'nodes_c' at the bottem
    K  = zeros(size(A));
    ct = length(nodes_c);
    for i=1:length(A)
        if ismember(i,nodes_c) %If this node has to be condensated
            K(length(K)-ct+1,:) = A(i,:); %Put it at the lower portion
            ct = ct-1;
        else
            K(i+ct-length(nodes_c),:) = A(i,:); %Else, put it at the top portion
        end
    end
end

function K = ReverseRestructureRows(A,nodes_c)
    %Perform inverse operation as "RestructureRows"
    K  = zeros(size(A));
    ct = length(nodes_c);
    for i=1:length(A)
        if ismember(i,nodes_c) %If this node was condensated
            K(i,:) = A(length(K)-ct+1,:); %Put it back to the top
            ct = ct-1;
        else
            K(i,:) = A(i+ct-length(nodes_c),:);
        end
    end
end


function [K,F] = ComputeStiffness(W,NrEls,sq_nr)
    %This function assembles the global stiffness matrix and force vector
    %for the square domain with width W, and with NrEls elements in x 
    %(and y) direction

    %Constant element stiffness matrix, compute once
    h = W/NrEls; %Element size
    Ke = ElementStiffness( h );

    %Allocate space for the global stiffness matrix and force vector
    K = zeros( (NrEls+1)*(NrEls+1) );
    F = zeros( (NrEls+1)*(NrEls+1),1 );

    %Assemble global stiffness matrix
    for hor=1:NrEls
        for ver=1:NrEls
            %Determine the node numbers associated to element [hor,ver] in the grid
            node1 = hor+(ver-1)*(NrEls+1);
            node2 = hor+(ver-1)*(NrEls+1)+1;
            node3 = hor+(ver)*(NrEls+1)+1;
            node4 = hor+(ver)*(NrEls+1);

            %Map and add the element stiffness matrix to the global stiffness matrix
            map = [node1, node2, node3, node4];
            for i=1:4
                for j=1:4
                    K( map(i),map(j) ) = K( map(i),map(j) ) + Ke(i,j);
                end
            end
        end
    end
    
    %Apply boundary conditions
    for node=1:(NrEls+1)*(NrEls+1)
        %Determine node coordinate
        node_x = (mod(node-1,NrEls+1))*(W/NrEls);
        node_y = floor((node-1)/(NrEls+1))*(W/NrEls);

        %Check if node on boundary, and determine boundary value
        [inside,bc] = BoundaryCondition(node_x,node_y,W,sq_nr);
        if inside
            F(:) = F(:) - K(:,node)*bc; %Move column to the right hand side
            K(node,:) = 0; %Cross out row
            K(:,node) = 0; %Cross out column
            K(node,node) = 1; %One on the diagonal
            F(node) = bc; %Boundary value on right hand side
        end
    end
    
    K = sparse(K);
end




function [inside,bc] = BoundaryCondition(x,y,W,sq_nr)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
    
    if (sq_nr == 1) && (x == 0) %On square number one, left boundary
        inside = true;
        bc = y;
    end
    if (sq_nr == 3) && (x == W) %On square number three, right boundary
        inside = true;
        bc = y;
    end
end

function Ke = ElementStiffness(h)
    %Compute the element stiffness matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element stiffness matrix to zeros
    Ke = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute grad-N matrix in global coordinates
            B  = dBasisfunction(GPlocs(iGP),GPlocs(jGP)) / J;
            % Integrate by summing the weighted contribution
            Ke = Ke + B*B'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function dN = dBasisfunction(xi,eta)
    %This function returns the gradient of the four linear basis functions
    %at point xi,eta in reference coordinates
    dN_xi1 = -0.25*(1-eta);
    dN_xi2 =  0.25*(1-eta);
    dN_xi3 =  0.25*(1+eta);
    dN_xi4 = -0.25*(1+eta);

    dN_eta1 = -0.25*(1-xi);
    dN_eta2 = -0.25*(1+xi);
    dN_eta3 =  0.25*(1+xi);
    dN_eta4 =  0.25*(1-xi);

    dN = [dN_xi1,dN_eta1; dN_xi2,dN_eta2; dN_xi3,dN_eta3; dN_xi4,dN_eta4];
end
