% Supporting material for the youtube lecture series
% "Model Order Reduction in Computational Solid Mechanics"
% accessible at youtube.com/playlist?list=PLMHTjE57oyvqbTrSo_e6x5rdGpp3ncgX8
% Author:                          Stein Stoter
% Email:                           k.f.s.stoter@tue.nl

function main

close all
clear
clc

%Input parameters
W = 4; % Width of the square domain
NrEls = 16; % Elements in X (and Y) direction


%Constant element stiffness matrix, compute once
h = W/NrEls; %Element size
Ke = ElementStiffness( h );

%Allocate space for the global stiffness matrix and force vector
K = zeros( (NrEls+1)*(NrEls+1) );
F = zeros( (NrEls+1)*(NrEls+1),1 );

%Assemble global stiffness matrix
for hor=1:NrEls
    for ver=1:NrEls
        %Determine the node numbers associated to element [hor,ver] in the grid
        node1 = hor+(ver-1)*(NrEls+1);
        node2 = hor+(ver-1)*(NrEls+1)+1;
        node3 = hor+(ver)*(NrEls+1)+1;
        node4 = hor+(ver)*(NrEls+1);
        
        %Map and add the element stiffness matrix to the global stiffness matrix
        map = [node1, node2, node3, node4];
        for i=1:4
            for j=1:4
                K( map(i),map(j) ) = K( map(i),map(j) ) + Ke(i,j);
            end
        end
        
    end
end

%Apply boundary conditions
for node=1:(NrEls+1)*(NrEls+1)
    %Determine node coordinate
    node_x = (mod(node-1,NrEls+1))*(W/NrEls);
    node_y = floor((node-1)/(NrEls+1))*(W/NrEls);
    
    %Check if node on boundary, and determine boundary value
    [inside,bc] = BoundaryCondition(node_x,node_y,W);
    if inside
        F(:) = F(:) - K(:,node)*bc; %Move column to the right hand side
        K(node,:) = 0; %Cross out row
        K(:,node) = 0; %Cross out column
        K(node,node) = 1; %One on the diagonal
        F(node) = bc; %Boundary value on right hand side
    end
end
K = sparse(K);

%Solve system of equation
u = K\F;

%Get exact solution and error
u_true = TrueSolution(W,NrEls);
u_error = u-u_true;

%Plot results
figure('Position', [200 200 1350 450])
subplot(1,3,1);
PlotRectangle(u,W,W,W/NrEls,0,0)
zlim([0,1])
title("Finite element solution")
subplot(1,3,2);
PlotRectangle(u_true,W,W,W/NrEls,0,0)
zlim([0,1])
title("Exact solution at nodes")
subplot(1,3,3);
PlotRectangle(u_error,W,W,W/NrEls,0,0)
title("Error at nodes")
end

function PlotRectangle(u,W,H,h,x_ll,y_ll)
    %Plot a solution vector 'u' on a rectangular domain WxH with element
    %size h and with lower-left coordinate (x_ll,y_ll) 
    [X,Y] = meshgrid( x_ll:h:x_ll+W , y_ll:h:y_ll+H );
    Z = reshape(u,size(X'))';
    surf(X,Y,Z)
end

function [inside,bc] = BoundaryCondition(x,y,W)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
    
    switch y
        case 0 %Southern boundary: zero value 
            inside = true;
            bc = 0;
        case W %Northern boundary: zero value
            inside = true;
            bc = 0;% -(x-W)*x;
    end
    switch x
        case W %Eastern boundary: zero value
            inside = true;
            bc =  0;
        case 0 %Western boundary: sine function
            inside = true;
            bc = sin(y*pi/W);
    end
end

function Ke = ElementStiffness(h)
    %Compute the element stiffness matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element stiffness matrix to zeros
    Ke = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute grad-N matrix in global coordinates
            B  = dBasisfunction(GPlocs(iGP),GPlocs(jGP)) / J;
            % Integrate by summing the weighted contribution
            Ke = Ke + B*B'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function u = TrueSolution(W,NrEls)
    %This function returns the exact solution for this problem as a vector
    %with nodal values corresponding to the finite element mesh
    u = zeros( (NrEls+1)*(NrEls+1),1 );
    for node=1:(NrEls+1)*(NrEls+1)
        %Determine node coordinate
        x = (mod(node-1,NrEls+1))*(W/NrEls);
        y = floor((node-1)/(NrEls+1))*(W/NrEls);
        %Obtain solution at this point
        b2 = 1/(1-exp(-2*pi));
        b1 = 1-b2;
        u(node) = sin(pi/W*y)*(b1*exp(pi/W*x)+b2*exp(-pi/W*x));
    end
end

function dN = dBasisfunction(xi,eta)
    %This function returns the gradient of the four linear basis functions
    %at point xi,eta in reference coordinates
    dN_xi1 = -0.25*(1-eta);
    dN_xi2 =  0.25*(1-eta);
    dN_xi3 =  0.25*(1+eta);
    dN_xi4 = -0.25*(1+eta);

    dN_eta1 = -0.25*(1-xi);
    dN_eta2 = -0.25*(1+xi);
    dN_eta3 =  0.25*(1+xi);
    dN_eta4 =  0.25*(1-xi);

    dN = [dN_xi1,dN_eta1; dN_xi2,dN_eta2; dN_xi3,dN_eta3; dN_xi4,dN_eta4];
end
