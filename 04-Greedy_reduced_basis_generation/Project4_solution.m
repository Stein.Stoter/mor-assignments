% Supporting material for the youtube lecture series
% "Model Order Reduction in Computational Solid Mechanics"
% accessible at youtube.com/playlist?list=PLMHTjE57oyvqbTrSo_e6x5rdGpp3ncgX8
% Author:                          Stein Stoter
% Email:                           k.f.s.stoter@tue.nl

function main

close all
clear
clc

%Input parameters
H = 4; % Height of the domain
NrElsVert = 32; % Elements in X (and Y) direction of square 1
NrBases = 5;

%Obtain the mass matrix, stiffness matrix and initial condition for this system
[M,K1,K2,K3,F1,F2,F3] = GetMatrices(H,NrElsVert);
M = sparse(M); K1 = sparse(K1); K2 = sparse(K2); K3 = sparse(K3);

%Precompute the reference stability coefficients
thetaB_prime = [1.5,3,1.5];
K_reference = thetaB_prime(1)*K1+thetaB_prime(2)*K2+thetaB_prime(3)*K3;
CB_prime = eigs(K_reference,M,1,'smallestabs');
lambdaM = eigs(M,1,'smallestabs');

%Greedy algorithm
P = max(5  ,NrBases);
Sampling = [1,0.2,2;  1,0.2,5;  1,0.2,2];
U = SolveSystem( 1, thetaB_prime, K1,K2,K3,F1,F2,F3); % Initial basis function
[U,ppoints] = addGreedyBasis( U, thetaB_prime,CB_prime,lambdaM ,Sampling,P, K1,K2,K3,F1,F2,F3 );

%Plot basis
for i = 1:min(5,P)
    figure('Position', [350*(i-1) 500+100*(-1)^i 500 200])
    PlotSolution(U(:,i),H,NrElsVert);
    title(strcat("Parameter point ", num2str( ppoints(i,:) )))
end
uiwait
close all

%Orthonormalization
U = orth(U);

%Plot basis
for i = 1:min(5,P)
    figure('Position', [350*(i-1) 500+100*(-1)^i 500 200])
    PlotSolution(U(:,i),H,NrElsVert);
    title(strcat("Orthonormalized basis ", num2str( i )))
end
uiwait
close all

%Construct the reduced basis finite element matrix formulation
Utilde = U(:,1:NrBases); %The reduced basis
[Kp1,Kp2,Kp3,Fp1,Fp2,Fp3] = getTildeMatrices( Utilde, K1,K2,K3,F1,F2,F3);

%Performing the optimization search, which is now very cheap
mu1 = 1;
[kappa_opt,kappa_range,sol_range] = getOptimum(mu1,Kp1,Kp2,Kp3,Fp1,Fp2,Fp3);
kappa1 = 1+kappa_opt; % Kappa 1 at the optimized design
kappa2 = 5-4*kappa_opt^3; % Kappa 2 at the optimized design
kappa3 = 1+kappa_opt^2; % Kappa 3 at the optimized design
["Optimal kappa*",kappa_opt] %Print kappa* at the optimized design

%Plot the output function (average temperature at the influx boundary) as a
%function of the input kappa*
figure()
plot(kappa_range,sol_range)
xlabel("kappa*")
ylabel("s(u(mu))")

%Solving and plotting the reduced basis solution at the optimal point
up = SolveSystem(mu1, [kappa1,kappa2,kappa3] ,Kp1,Kp2,Kp3,Fp1,Fp2,Fp3);
u_RB = Utilde*up;
figure('Position', [200 200 850 350])
PlotSolution(u_RB,H,NrElsVert)
title("RB solution")

%Plotting the error of the RB solution
u_truth = SolveSystem(mu1, [kappa1,kappa2,kappa3] ,K1,K2,K3,F1,F2,F3);
error = u_RB-u_truth;
figure('Position', [500 200 850 350])
PlotSolution(error,H,NrElsVert)
title("Error")

%Estimated and true error at parameter location
e_est = getError(Utilde, thetaB_prime,CB_prime,lambdaM, [kappa1,kappa2,kappa3], K1,K2,K3,F1,F2,F3, Kp1,Kp2,Kp3,Fp1,Fp2,Fp3)
e_real = (u_truth-u_RB)'*M*(u_truth-u_RB)

end




function [U,thetaB] = addGreedyBasis( U, thetaB,CB_prime,lambdaM ,S,P,  K1,K2,K3,F1,F2,F3)
    thetaB_prime = thetaB(1,:);
    [Kp1,Kp2,Kp3,Fp1,Fp2,Fp3] = getTildeMatrices( U, K1,K2,K3,F1,F2,F3);
    
    k1 = 0; k2 = 0; k3 = 0;
    maxerror = 0;
    tic
    for kappa1 = S(1,1):S(1,2):S(1,3)
        for kappa2 = S(2,1):S(2,2):S(2,3)
            for kappa3 = S(3,1):S(3,2):S(3,3)
                ppoint = [kappa1,kappa2,kappa3];
                e = getError(U, thetaB_prime,CB_prime,lambdaM, ppoint, K1,K2,K3,F1,F2,F3,  Kp1,Kp2,Kp3,Fp1,Fp2,Fp3);
                if e > maxerror
                    maxerror = e;
                    k1 = kappa1; k2 = kappa2; k3 = kappa3;
                end
            end
        end
    end
    toc
    ppoint = [k1,k2,k3];
    U = [U , SolveSystem(1,ppoint,K1,K2,K3,F1,F2,F3) ];
    thetaB = [thetaB; ppoint];
    
    if P > 1
        [U,thetaB] = addGreedyBasis( U,thetaB,CB_prime,lambdaM ,S,P-1,  K1,K2,K3,F1,F2,F3);
    end
end

function e = getError(Utilde, thetaB_prime,CB_prime,lambdaM, ppoint, K1,K2,K3,F1,F2,F3, Kp1,Kp2,Kp3,Fp1,Fp2,Fp3)
    F = F1+F2+F3;
    wtilde = SolveSystem(1,ppoint, Kp1,Kp2,Kp3,Fp1,Fp2,Fp3);
    utilde = Utilde*wtilde;
    Ftilde = (ppoint(1)*K1+ppoint(2)*K2+ppoint(3)*K3)*utilde;
    r = F-Ftilde;
    
    CB = CB_prime * min( [ppoint(1)/thetaB_prime(1),ppoint(2)/thetaB_prime(2),ppoint(3)/thetaB_prime(3)] );
    gammaR = sqrt(  r'*r/lambdaM  ) ;
    
    e = gammaR/CB;
end

function [Kp1,Kp2,Kp3,Fp1,Fp2,Fp3] = getTildeMatrices( Utilde, K1,K2,K3,F1,F2,F3)
    Kp1 = getKtilde(K1,Utilde); %Determine K1-tilde matrix
    Kp2 = getKtilde(K2,Utilde); %Determine K2-tilde matrix
    Kp3 = getKtilde(K3,Utilde); %Determine K3-tilde matrix
    Fp1 = getFtilde(F1,Utilde); %Determine F1-tilde matrix
    Fp2 = getFtilde(F2,Utilde); %Determine F2-tilde matrix
    Fp3 = getFtilde(F3,Utilde); %Determine F3-tilde matrix
end
    
function Ktilde = getKtilde(K,Utilde)
    %Obtain stiffness matrix in the new basis representation
    %(See project 2 description for theory)
    Ktilde = Utilde'*K*Utilde;
end

function Ftilde = getFtilde(F,Utilde)
    %Obtain force vector in the new basis representation
    %(See project 2 description for theory)
    Ftilde = Utilde'*F;
end

function [kappa_opt,kappa_range,sol_range] = getOptimum(mu1,K1,K2,K3,F1,F2,F3)
    %This function computed the output for the range of kappa* for very
    %small increments of kappa. This is possible since the reduced basis
    %implementation is only 5x5 and thus incredibly cheap to solve. This
    %would not be feasible with the truth implementation
    
    dkappa = 0.001; 
    kappa_range = 0:dkappa:1;
    sol_range = zeros(size(kappa_range));
    ct = 1;
    kappa_opt = 0;
    s_p = 10000;
    for kappa = kappa_range
        %Obtain kappa's corresponding to this kappa*
        kappa1 = 1+kappa;
        kappa2 = 5-4*kappa^3;
        kappa3 = 1+kappa^2;
        ppoint = [kappa1,kappa2,kappa3];
        %Solve the system for this design
        s_n = getOutput(mu1, ppoint ,K1,K2,K3,F1,F2,F3);
        if s_p < s_n && kappa_opt==0 %Optimal point if output starts to increase
            kappa_opt = kappa-dkappa;
        end
        s_p = s_n;
        sol_range(ct) = s_n; %Store the output
        ct = ct+1;
    end
end

function s = getOutput(mu1, ppoint ,K1,K2,K3,F1,F2,F3)
    u = SolveSystem(mu1, ppoint ,K1,K2,K3,F1,F2,F3);
    %Use the compliant property of the output
    s = 1/4.*F1'*u;
end

function u = SolveSystem(mu1,thetaB,K1,K2,K3,F1,F2,F3)
    kappa1 = thetaB(1); kappa2 = thetaB(2); kappa3 = thetaB(3);
    %Affine combination of the individual siffness matrix and force vector
    K = kappa1*K1+kappa2*K2+kappa3*K3;
    F = mu1*F1+F2+F3;
    
    %Solve the system
    u = K\F;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function PlotSolution(u,H,NrElsVert)
    %Restructure the solution vectors
    [u1,u2,u3] = SubdivideVector( u ,NrElsVert);
    
    PlotRectangle(u1,H,H, H/NrElsVert, 0,0)
    hold on
    PlotRectangle(u2,H/4,H/4, H/NrElsVert, H,0)
    hold on
    PlotRectangle(u3,H,H, H/NrElsVert, H+H/4,0)
end

function PlotRectangle(u,W,H,h,x_ll,y_ll)
    %Plot a solution vector 'u' on a rectangular domain WxH with element
    %size h and with lower-left coordinate (x_ll,y_ll) 
    [X,Y] = meshgrid( x_ll:h:x_ll+W , y_ll:h:y_ll+H );
    Z = reshape(u,size(X'))';
    surf(X,Y,Z)
end



function [M,K1,K2,K3,F1,F2,F3] = GetMatrices(H,NrElsVert)
    [Ma,Ka,Fa] = ComputeMatricesSquare(H,NrElsVert,1);
    [Mb,Kb,Fb] = ComputeMatricesSquare(H/4,NrElsVert/4,2);
    [Mc,Kc,Fc] = ComputeMatricesSquare(H,NrElsVert,3);

    K1 = CombineMatrices(Ka,zeros(size(Kb)),zeros(size(Kc)),NrElsVert);
    F1 = CombineMatrices(Fa,zeros(size(Fb)),zeros(size(Fc)),NrElsVert);
    
    K2 = CombineMatrices(zeros(size(Ka)),Kb,zeros(size(Kc)),NrElsVert);
    F2 = CombineMatrices(zeros(size(Fa)),Fb,zeros(size(Fc)),NrElsVert);
    
    K3 = CombineMatrices(zeros(size(Ka)),zeros(size(Kb)),Kc,NrElsVert);
    F3 = CombineMatrices(zeros(size(Fa)),zeros(size(Fb)),Fc,NrElsVert);
    
    M = CombineMatrices(Ma,Mb,Mc,NrElsVert);
end
    
function A = CombineMatrices(A1,A2,A3,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Combine matrices
    Awidth = size(A1,2);
    if Awidth == 1 % Dealing with vectors
        A = zeros(TotDofs,1);
        A(1:Dofs1,1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,1) = A3;
        A(map_square2,1) = A(map_square2,1)+A2;
    else
        A = zeros(TotDofs,TotDofs);
        A(1:Dofs1,1:Dofs1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,TotDofs-Dofs1+1:TotDofs) = A3;
        A(map_square2,map_square2) = A(map_square2,map_square2)+A2;
    end
end

function [u1,u2,u3] = SubdivideVector( u ,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Splice vector
    u1 = u(1:Dofs1,1);
    u2 = u(map_square2,1);
    u3 = u(TotDofs-Dofs1+1:TotDofs,1);
end


function [M,K,F] = ComputeMatricesSquare(W,NrEls,sq_nr)
    %This function assembles the global mass and stiffness matrix 
    %for the square domain with width W, and with NrEls elements in x 
    %(and y) direction

    %Constant element stiffness matrix, compute once
    h = W/NrEls; %Element size
    Me = ElementMass( h );
    Ke = ElementStiffness( h );

    %Allocate space for the global mass and stiffness matrix
    M = zeros( (NrEls+1)*(NrEls+1) );
    K = zeros( (NrEls+1)*(NrEls+1) );
    F = zeros( (NrEls+1)*(NrEls+1),1 );

    %Assemble global stiffness matrix
    for hor=1:NrEls
        for ver=1:NrEls
            %Determine the node numbers associated to element [hor,ver] in the grid
            node1 = hor+(ver-1)*(NrEls+1);
            node2 = hor+(ver-1)*(NrEls+1)+1;
            node3 = hor+(ver)*(NrEls+1)+1;
            node4 = hor+(ver)*(NrEls+1);

            %Map and add the element stiffness matrix to the global stiffness matrix
            map = [node1, node2, node3, node4];
            for i=1:4
                for j=1:4
                    M( map(i),map(j) ) = M( map(i),map(j) ) + Me(i,j);
                    K( map(i),map(j) ) = K( map(i),map(j) ) + Ke(i,j);
                end
            end
        end
    end
    
    %Apply boundary conditions
    for node=1:(NrEls+1)*(NrEls+1)
        %Determine node coordinate
        node_x = (mod(node-1,NrEls+1))*(W/NrEls);
        node_y = floor((node-1)/(NrEls+1))*(W/NrEls);

        %Check if node on boundary, and determine boundary value
        [inside,bc] = DirichletBoundaryCondition(node_x,node_y,W,sq_nr);
        if inside
            F(:) = F(:) - K(:,node)*bc; %Move column to the right hand side
            K(node,:) = 0; %Cross out row
            K(:,node) = 0; %Cross out column
            K(node,node) = 1; %One on the diagonal
            F(node) = bc; %Boundary value on right hand side
        end
        %Check if node on influx Neumann, and determine boundary value
        [inside,bc] = NeumannBoundaryCondition(node_x,node_y,W,sq_nr);
        if inside
            F(node) = F(node)+bc*W/NrEls; %Boundary value on right hand side
        end
    end
end

function [inside,bc] = DirichletBoundaryCondition(x,y,W,sq_nr)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
    
    if (sq_nr == 3) && (x == W) %On square number three, right boundary
        inside = true;
        bc = 0;
    end
end

function [inside,bc] = NeumannBoundaryCondition(x,y,W,sq_nr)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
															   
					 
    
    if (sq_nr == 1) && (x == 0) %On square number one, left boundary
        inside = true;
        bc = 1;
        if y==0 || y==W
            bc = 0.5;
												   
													
														
															
															 
        end
    end
end

function Me = ElementMass(h)
    %Compute the element mass matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element mass matrix to zeros
    Me = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute N matrix in global coordinates
            N  = Basisfunction(GPlocs(iGP),GPlocs(jGP));
            % Integrate by summing the weighted contribution
            Me = Me + N*N'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function Ke = ElementStiffness(h)
    %Compute the element stiffness matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element stiffness matrix to zeros
    Ke = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute grad-N matrix in global coordinates
            B  = dBasisfunction(GPlocs(iGP),GPlocs(jGP)) / J;
            % Integrate by summing the weighted contribution
            Ke = Ke + B*B'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function N = Basisfunction(xi,eta)
    %This function returns the value of the four linear basis functions
    %at point xi,eta in reference coordinates
    N1 = 0.25*(1-xi).*(1-eta);
    N2 = 0.25*(1+xi).*(1-eta);
    N3 = 0.25*(1+xi).*(1+eta);
    N4 = 0.25*(1-xi).*(1+eta);
    
    N = [N1;N2;N3;N4];
end

function dN = dBasisfunction(xi,eta)
    %This function returns the gradient of the four linear basis functions
    %at point xi,eta in reference coordinates
    dN_xi1 = -0.25*(1-eta);
    dN_xi2 =  0.25*(1-eta);
    dN_xi3 =  0.25*(1+eta);
    dN_xi4 = -0.25*(1+eta);

    dN_eta1 = -0.25*(1-xi);
    dN_eta2 = -0.25*(1+xi);
    dN_eta3 =  0.25*(1+xi);
    dN_eta4 =  0.25*(1-xi);

    dN = [dN_xi1,dN_eta1; dN_xi2,dN_eta2; dN_xi3,dN_eta3; dN_xi4,dN_eta4];
end
