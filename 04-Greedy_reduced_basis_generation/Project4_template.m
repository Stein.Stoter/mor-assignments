% Supporting material for the youtube lecture series
% "Model Order Reduction in Computational Solid Mechanics"
% accessible at youtube.com/playlist?list=PLMHTjE57oyvqbTrSo_e6x5rdGpp3ncgX8
% Author:                          Stein Stoter
% Email:                           k.f.s.stoter@tue.nl

function main

close all
clear
clc

%Input parameters
H = 4; % Height of the domain
NrElsVert = 32; % Elements in X (and Y) direction of square 1

%Obtain the mass matrix, stiffness matrix and initial condition for this system
[M,K1,K2,K3,F1,F2,F3] = GetMatrices(H,NrElsVert);
M = sparse(M); K1 = sparse(K1); K2 = sparse(K2); K3 = sparse(K3);

%Example construction:
K_example = K1+K2+K3;
F_example = F1+F2+F3;
u_example = K_example\F_example;
u_L2_examle = u_example'*M*u_example

%Example plot solution of the initial condition
figure('Position', [200 200 950 400])
PlotSolution(u_example,H,NrElsVert)




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add your code here.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function PlotSolution(u,H,NrElsVert)
    %Restructure the solution vectors
    [u1,u2,u3] = SubdivideVector( u ,NrElsVert);
    
    PlotRectangle(u1,H,H, H/NrElsVert, 0,0)
    hold on
    PlotRectangle(u2,H/4,H/4, H/NrElsVert, H,0)
    hold on
    PlotRectangle(u3,H,H, H/NrElsVert, H+H/4,0)
end

function PlotRectangle(u,W,H,h,x_ll,y_ll)
    %Plot a solution vector 'u' on a rectangular domain WxH with element
    %size h and with lower-left coordinate (x_ll,y_ll) 
    [X,Y] = meshgrid( x_ll:h:x_ll+W , y_ll:h:y_ll+H );
    Z = reshape(u,size(X'))';
    surf(X,Y,Z)
end



function [M,K1,K2,K3,F1,F2,F3] = GetMatrices(H,NrElsVert)
    [Ma,Ka,Fa] = ComputeMatricesSquare(H,NrElsVert,1);
    [Mb,Kb,Fb] = ComputeMatricesSquare(H/4,NrElsVert/4,2);
    [Mc,Kc,Fc] = ComputeMatricesSquare(H,NrElsVert,3);

    K1 = CombineMatrices(Ka,zeros(size(Kb)),zeros(size(Kc)),NrElsVert);
    F1 = CombineMatrices(Fa,zeros(size(Fb)),zeros(size(Fc)),NrElsVert);
    
    K2 = CombineMatrices(zeros(size(Ka)),Kb,zeros(size(Kc)),NrElsVert);
    F2 = CombineMatrices(zeros(size(Fa)),Fb,zeros(size(Fc)),NrElsVert);
    
    K3 = CombineMatrices(zeros(size(Ka)),zeros(size(Kb)),Kc,NrElsVert);
    F3 = CombineMatrices(zeros(size(Fa)),zeros(size(Fb)),Fc,NrElsVert);
    
    M = CombineMatrices(Ma,Mb,Mc,NrElsVert);
end
    
function A = CombineMatrices(A1,A2,A3,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Combine matrices
    Awidth = size(A1,2);
    if Awidth == 1 % Dealing with vectors
        A = zeros(TotDofs,1);
        A(1:Dofs1,1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,1) = A3;
        A(map_square2,1) = A(map_square2,1)+A2;
    else
        A = zeros(TotDofs,TotDofs);
        A(1:Dofs1,1:Dofs1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,TotDofs-Dofs1+1:TotDofs) = A3;
        A(map_square2,map_square2) = A(map_square2,map_square2)+A2;
    end
end

function [u1,u2,u3] = SubdivideVector( u ,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Splice vector
    u1 = u(1:Dofs1,1);
    u2 = u(map_square2,1);
    u3 = u(TotDofs-Dofs1+1:TotDofs,1);
end


function [M,K,F] = ComputeMatricesSquare(W,NrEls,sq_nr)
    %This function assembles the global mass and stiffness matrix 
    %for the square domain with width W, and with NrEls elements in x 
    %(and y) direction

    %Constant element stiffness matrix, compute once
    h = W/NrEls; %Element size
    Me = ElementMass( h );
    Ke = ElementStiffness( h );

    %Allocate space for the global mass and stiffness matrix
    M = zeros( (NrEls+1)*(NrEls+1) );
    K = zeros( (NrEls+1)*(NrEls+1) );
    F = zeros( (NrEls+1)*(NrEls+1),1 );

    %Assemble global stiffness matrix
    for hor=1:NrEls
        for ver=1:NrEls
            %Determine the node numbers associated to element [hor,ver] in the grid
            node1 = hor+(ver-1)*(NrEls+1);
            node2 = hor+(ver-1)*(NrEls+1)+1;
            node3 = hor+(ver)*(NrEls+1)+1;
            node4 = hor+(ver)*(NrEls+1);

            %Map and add the element stiffness matrix to the global stiffness matrix
            map = [node1, node2, node3, node4];
            for i=1:4
                for j=1:4
                    M( map(i),map(j) ) = M( map(i),map(j) ) + Me(i,j);
                    K( map(i),map(j) ) = K( map(i),map(j) ) + Ke(i,j);
                end
            end
        end
    end
    
    %Apply boundary conditions
    for node=1:(NrEls+1)*(NrEls+1)
        %Determine node coordinate
        node_x = (mod(node-1,NrEls+1))*(W/NrEls);
        node_y = floor((node-1)/(NrEls+1))*(W/NrEls);

        %Check if node on boundary, and determine boundary value
        [inside,bc] = DirichletBoundaryCondition(node_x,node_y,W,sq_nr);
        if inside
            F(:) = F(:) - K(:,node)*bc; %Move column to the right hand side
            K(node,:) = 0; %Cross out row
            K(:,node) = 0; %Cross out column
            K(node,node) = 1; %One on the diagonal
            F(node) = bc; %Boundary value on right hand side
        end
    end
end

function [inside,bc] = DirichletBoundaryCondition(x,y,W,sq_nr)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
    
    if (sq_nr == 1) && (x == 0) %On square number three, right boundary
        inside = true;
        bc = y;
    end
    
    if (sq_nr == 3) && (x == W) %On square number three, right boundary
        inside = true;
        bc = 0;
    end
end

function Me = ElementMass(h)
    %Compute the element mass matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element mass matrix to zeros
    Me = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute N matrix in global coordinates
            N  = Basisfunction(GPlocs(iGP),GPlocs(jGP));
            % Integrate by summing the weighted contribution
            Me = Me + N*N'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function Ke = ElementStiffness(h)
    %Compute the element stiffness matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element stiffness matrix to zeros
    Ke = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute grad-N matrix in global coordinates
            B  = dBasisfunction(GPlocs(iGP),GPlocs(jGP)) / J;
            % Integrate by summing the weighted contribution
            Ke = Ke + B*B'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function N = Basisfunction(xi,eta)
    %This function returns the value of the four linear basis functions
    %at point xi,eta in reference coordinates
    N1 = 0.25*(1-xi).*(1-eta);
    N2 = 0.25*(1+xi).*(1-eta);
    N3 = 0.25*(1+xi).*(1+eta);
    N4 = 0.25*(1-xi).*(1+eta);
    
    N = [N1;N2;N3;N4];
end

function dN = dBasisfunction(xi,eta)
    %This function returns the gradient of the four linear basis functions
    %at point xi,eta in reference coordinates
    dN_xi1 = -0.25*(1-eta);
    dN_xi2 =  0.25*(1-eta);
    dN_xi3 =  0.25*(1+eta);
    dN_xi4 = -0.25*(1+eta);

    dN_eta1 = -0.25*(1-xi);
    dN_eta2 = -0.25*(1+xi);
    dN_eta3 =  0.25*(1+xi);
    dN_eta4 =  0.25*(1-xi);

    dN = [dN_xi1,dN_eta1; dN_xi2,dN_eta2; dN_xi3,dN_eta3; dN_xi4,dN_eta4];
end
