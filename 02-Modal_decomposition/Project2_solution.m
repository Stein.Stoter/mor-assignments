% Supporting material for the youtube lecture series
% "Model Order Reduction in Computational Solid Mechanics"
% accessible at youtube.com/playlist?list=PLMHTjE57oyvqbTrSo_e6x5rdGpp3ncgX8
% Author:                          Stein Stoter
% Email:                           k.f.s.stoter@tue.nl

function main

close all
clear
clc

%Input parameters
H = 4; % Height of the domain
NrElsVert = 16; % Elements in X (and Y) direction of square 1

%Obtain the mass matrix, stiffness matrix and unitial condition for this system
[M,K,u_ic] = ComputeMatrices(H,NrElsVert);

%User variables
Prange = [1,3,5,7];
T = 3;
Dt = 0.1;
dt = 0.01;

%Perform time stepping on Truth model as a reference
[Utrue,t] = PerformTimeStepping(M,K,u_ic, T,Dt,dt);
PlotAnimation(Utrue,H,NrElsVert, "Truth solution")

%Determine eigenvectors
[V,D] = eigs(K,M,max(Prange),'smallestabs');

%Plot eigensolutions
for i = 1:7
    figure('Position', [350*(i-1) 500+100*(-1)^i 500 200])
    PlotSolution(V(:,i),H,NrElsVert)
    title(strcat("Mode number ", num2str(i)))
end
uiwait
close all

%Allocate memory for data of interest
ICs = zeros( [length(u_ic),length(Prange)+1] ); %Initial conditions in the various bases representations
ICs(:,1) = u_ic;
U_L2_p = zeros( [length(Prange),length(t)] ); %L2 time behavior for the various bases

%For each of the P-number of modal bases, perform the analysis
for i = 1:length(Prange)
    P = Prange(i);
    Mp = eye(P); %Determine M-tilde matrix
    Kp = D(1:P,1:P); %Determine K-tilde matrix
    u_icp = V(:,1:P)'*M*u_ic; %Determine initial condition in this basis
    
    %Perform the time stepping and save the L2 and IC results
    [Up,t] = PerformTimeStepping(Mp,Kp,u_icp, T,Dt,dt );
    U = V(:,1:P)*Up;
    ICs(:,i+1) = U(:,1);
    U_L2_p(i,:) = ComputeL2(U,M);
    
    %Show the animated solution
    PlotAnimation(U,H,NrElsVert, strcat("Using ",  num2str(Prange(i)), " modes"))
end

%Show the initial conditions in the different bases
figure('Position', [0 600 500 200])
PlotSolution(ICs(:,1),H,NrElsVert);
zlim([0,max(u_ic)])
title("Truth initial condition")
for i = 2:size(ICs,2)
    figure('Position', [350*(i-1) 700+100*(-1)^i 500 200])
    PlotSolution(ICs(:,i),H,NrElsVert);
    zlim([0,max(u_ic)])
    title(strcat("Initial condition represented by ", num2str(Prange(i-1)), " modes"))
end

%Plot the time behavior of the L2 norm of u
U_L2_true = ComputeL2(Utrue,M);
figure('Position', [200 200 800 400])
plot(t,U_L2_true,'LineWidth',2)
L = {"Truth solution"};
for i = 1:length(Prange)
    hold on
    plot(t,U_L2_p(i,:))
    L{i+1} = strcat(num2str(Prange(i))," modes");
end
title('L2 norm over time')
legend(L)
xlabel("t")
ylabel("||u||")
uiwait
close all

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%









function [U,ts] = PerformTimeStepping(M,K,u_ic,T,Dt,dt)
    %Perform the time stepping using Crank-Nicolson
    % Use small time steps first, and bigger later
    ts = [0:dt:Dt-dt Dt:Dt:T];
    
    %Allocate memory
    U = zeros( [length(u_ic),length(ts)] );
    U(:,1) = u_ic;
    
    %Determine the advancement matrices for the two time step sizes
    CrankNicolsonMatrix_dt = (M+dt/2*K)\(M-dt/2*K);
    CrankNicolsonMatrix_Dt = (M+Dt/2*K)\(M-Dt/2*K);
    
    ct = 2;
    u = u_ic;
    for t = ts(2:end)
        if t > Dt %Split in small and large time steps
            u = CrankNicolsonMatrix_Dt*u;
        else 
            u = CrankNicolsonMatrix_dt*u;
        end
        U(:,ct) = u;
        ct = ct+1;
    end
end

function PlotAnimation(U,H,NrElsVert, titl)
    %Initiate plot
    figure('Position', [200 200 1150 450])
    title(titl)
    zmax = max(U(:,1));
    zlim([0,zmax])
    for i=1:size(U,2)
        u = U(:,i);
        clf
        PlotSolution(u,H,NrElsVert)
        title(titl)
        zlim([0,zmax])
        drawnow
    end
    uiwait
end

function U_L2 = ComputeL2(U,M)
    %Given U, a matrix of solutions (coloms vectors) for different times
    %and M, the mass matrix, compute the L2 integral
    U_L2 = zeros(size(U,2),1);
    for i =1:size(U,2)
        U_L2(i) = sqrt(U(:,i)'*M*U(:,i));
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function PlotSolution(u,H,NrElsVert)
    %Restructure the solution vectors
    [u1,u2,u3] = SubdivideVector( u ,NrElsVert);
    
    PlotRectangle(u1,H,H, H/NrElsVert, 0,0)
    hold on
    PlotRectangle(u2,H/4,H/4, H/NrElsVert, H,0)
    hold on
    PlotRectangle(u3,H,H, H/NrElsVert, H+H/4,0)
end

function PlotRectangle(u,W,H,h,x_ll,y_ll)
    %Plot a solution vector 'u' on a rectangular domain WxH with element
    %size h and with lower-left coordinate (x_ll,y_ll) 
    [X,Y] = meshgrid( x_ll:h:x_ll+W , y_ll:h:y_ll+H );
    Z = reshape(u,size(X'))';
    surf(X,Y,Z)
end



function [M,K,u_ic] = ComputeMatrices(H,NrElsVert)
    %Get global mass and stiffness matrices and force vector
    [M1,K1,F1] = ComputeMatricesSquare(H,NrElsVert,1);
    [M2,K2,F2] = ComputeMatricesSquare(H/4,NrElsVert/4,2);
    [M3,K3,F3] = ComputeMatricesSquare(H,NrElsVert,3);

    %Combine the systems of equations
    M = CombineMatrices(M1,M2,M3,NrElsVert);
    K = CombineMatrices(K1,K2,K3,NrElsVert);
    F = CombineMatrices(F1,F2,F3,NrElsVert);
    
    %Obtain initial condition
    u_ic = K\F;
    
    %Zero values on the boundary
    TotDofs = (NrElsVert+1)*(NrElsVert+1)*2+(NrElsVert/4-1)*(NrElsVert/4+1);
    u_ic(1:NrElsVert+1:(NrElsVert+1)*(NrElsVert+1)) = 0;
    u_ic(TotDofs:-(NrElsVert+1):TotDofs-(NrElsVert+1)*(NrElsVert)) = 0;
    
    K = sparse(K);
    M = sparse(M);
end

function A = CombineMatrices(A1,A2,A3,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Combine matrices
    Awidth = size(A1,2);
    if Awidth == 1 % Dealing with vectors
        A = zeros(TotDofs,1);
        A(1:Dofs1,1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,1) = A3;
        A(map_square2,1) = A(map_square2,1)+A2;
    else
        A = zeros(TotDofs,TotDofs);
        A(1:Dofs1,1:Dofs1) = A1;
        A(TotDofs-Dofs1+1:TotDofs,TotDofs-Dofs1+1:TotDofs) = A3;
        A(map_square2,map_square2) = A(map_square2,map_square2)+A2;
    end
end

function [u1,u2,u3] = SubdivideVector( u ,NrEls)
    %Determine the connectivity nodes 
    NrEls2 = NrEls/4; % Elements in X (and Y) direction of square 2
    TotDofs = (NrEls+1)*(NrEls+1)*2+(NrEls2-1)*(NrEls2+1);
    Dofs1 = (NrEls+1)*(NrEls+1);
    Dofs2 = (NrEls2+1)*(NrEls2+1);
    nodes_c1_r = NrEls+1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_l = 1:NrEls2+1:Dofs2;
    nodes_c2_r = NrEls2+1:NrEls2+1:Dofs2;
    nodes_c3_l = 1:NrEls+1:(NrEls+1)*(NrEls2+1);
    nodes_c2_c = 1:Dofs2;
    nodes_c2_lr = horzcat(nodes_c2_l,nodes_c2_r);
    nodes_c2_c(nodes_c2_lr) = [];
    
    %Squares 1 and 3 map easily to the global domain, but square 2 requires
    %a more sophisticated mapping
    offset2 = Dofs1+1;
    offset3 = TotDofs-Dofs1;
    map_square2 = zeros(1,Dofs2); %Map square-local nodes to domain-global nodes
    map_square2(nodes_c2_l) = nodes_c1_r; %Map left side of Sq1 to right side of Sq2
    map_square2(nodes_c2_c) = offset2:offset3; %Map center to remaining nodes
    map_square2(nodes_c2_r) = nodes_c3_l+offset3; %Map left side of Sq1 to right side of Sq2
    
    %Splice vector
    u1 = u(1:Dofs1,1);
    u2 = u(map_square2,1);
    u3 = u(TotDofs-Dofs1+1:TotDofs,1);
end


function [M,K,F] = ComputeMatricesSquare(W,NrEls,sq_nr)
    %This function assembles the global mass and stiffness matrix 
    %for the square domain with width W, and with NrEls elements in x 
    %(and y) direction

    %Constant element stiffness matrix, compute once
    h = W/NrEls; %Element size
    Me = ElementMass( h );
    Ke = ElementStiffness( h );

    %Allocate space for the global mass and stiffness matrix
    M = zeros( (NrEls+1)*(NrEls+1) );
    K = zeros( (NrEls+1)*(NrEls+1) );
    F = zeros( (NrEls+1)*(NrEls+1),1 );

    %Assemble global stiffness matrix
    for hor=1:NrEls
        for ver=1:NrEls
            %Determine the node numbers associated to element [hor,ver] in the grid
            node1 = hor+(ver-1)*(NrEls+1);
            node2 = hor+(ver-1)*(NrEls+1)+1;
            node3 = hor+(ver)*(NrEls+1)+1;
            node4 = hor+(ver)*(NrEls+1);

            %Map and add the element stiffness matrix to the global stiffness matrix
            map = [node1, node2, node3, node4];
            for i=1:4
                for j=1:4
                    M( map(i),map(j) ) = M( map(i),map(j) ) + Me(i,j);
                    K( map(i),map(j) ) = K( map(i),map(j) ) + Ke(i,j);
                end
            end
        end
    end
    
    %Apply boundary conditions
    for node=1:(NrEls+1)*(NrEls+1)
        %Determine node coordinate
        node_x = (mod(node-1,NrEls+1))*(W/NrEls);
        node_y = floor((node-1)/(NrEls+1))*(W/NrEls);

        %Check if node on boundary, and determine boundary value
        [inside,bc] = BoundaryCondition(node_x,node_y,W,sq_nr);
        if inside
            %Zero acceleration on Dirichlet nodes
            M(node,:) = 0; %Cross out row
            M(:,node) = 0; %Cross out column
            M(node,node) = 0; %One on the diagonal
            %Prescribed displacement on Dirichlet nodes
            F(:) = F(:) - K(:,node)*bc; %Move column to the right hand side
            K(node,:) = 0; %Cross out row
            K(:,node) = 0; %Cross out row
            K(node,node) = 1; %One on the diagonal
            F(node) = bc; %Zero boundary value on right hand side
        end
    end
    
    K = sparse(K);
    M = sparse(M);
end

function [inside,bc] = BoundaryCondition(x,y,W,sq_nr)
    %This function gets an x,y coordinate and the domain width and returns:
    % - true/false whether this point lies on the (Dirichlet) boundary (="inside")
    % - value of the boundary condition at this point (="bc")
    
    %default values: not on boundary
    inside = false;
    bc = -1;
    
    if (sq_nr == 1) && (x == 0) %On square number one, left boundary
        inside = true;
        bc = y;
    end
    if (sq_nr == 3) && (x == W) %On square number three, right boundary
        inside = true;
        bc = y;
    end
end

function Me = ElementMass(h)
    %Compute the element mass matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element mass matrix to zeros
    Me = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute N matrix in global coordinates
            N  = Basisfunction(GPlocs(iGP),GPlocs(jGP));
            % Integrate by summing the weighted contribution
            Me = Me + N*N'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function Ke = ElementStiffness(h)
    %Compute the element stiffness matrix of a single square element with
    %width h, and four linear nodal basis functions
    
    J = h/2; %Jacobian of square element

    % Coordinates and weights of Gaussian quadrature points in 2x2 reference frame
    GPlocs   = [-5.773502691896258e-01, 5.773502691896258e-01];
    GPweight = [1,1];
    
    %Set element stiffness matrix to zeros
    Ke = zeros(4);

    %Loop over quadrature points
    for jGP=1:length(GPlocs) %Gauss points in y
        for iGP=1:length(GPlocs) %Gauss points in x
            % Compute grad-N matrix in global coordinates
            B  = dBasisfunction(GPlocs(iGP),GPlocs(jGP)) / J;
            % Integrate by summing the weighted contribution
            Ke = Ke + B*B'*J*J * GPweight(iGP)*GPweight(jGP);
        end
    end
end

function N = Basisfunction(xi,eta)
    %This function returns the value of the four linear basis functions
    %at point xi,eta in reference coordinates
    N1 = 0.25*(1-xi).*(1-eta);
    N2 = 0.25*(1+xi).*(1-eta);
    N3 = 0.25*(1+xi).*(1+eta);
    N4 = 0.25*(1-xi).*(1+eta);
    
    N = [N1;N2;N3;N4];
end

function dN = dBasisfunction(xi,eta)
    %This function returns the gradient of the four linear basis functions
    %at point xi,eta in reference coordinates
    dN_xi1 = -0.25*(1-eta);
    dN_xi2 =  0.25*(1-eta);
    dN_xi3 =  0.25*(1+eta);
    dN_xi4 = -0.25*(1+eta);

    dN_eta1 = -0.25*(1-xi);
    dN_eta2 = -0.25*(1+xi);
    dN_eta3 =  0.25*(1+xi);
    dN_eta4 =  0.25*(1-xi);

    dN = [dN_xi1,dN_eta1; dN_xi2,dN_eta2; dN_xi3,dN_eta3; dN_xi4,dN_eta4];
end
